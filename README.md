# README #

## WASP Technology
### WASP-N
is a standalone bridge that allows Bluetooth Smart® (formally Bluetooth Low Energy or BLE) and ANT+ devices to communicate wirelessly through Wi-Fi networks to other devices or over the Internet. For example BLE/ANT+ heart rate monitors, home scales, pulse-oximeter monitors, and blood glucose monitors are all able to use this bridge module to communicate their data to central monitoring stations via the WiFi network. View product brief.

### WASP-POE-2 & 4 
bridges allow for Bluetooth Smart and ANT/ANT+ sensor devices to communicate messages directly onto a wired Ethernet network and forwards the data to end points on the local Ethernet network. This bridge is powered directly from the Ethernet connection through a POE-enabled router or through an optional POE-injector. View product brief.

### WASP-N
is ideal for using in different situations where mobility and wireless access are important. WASP-POE-2 & 3 are ideal for use in a wired Ethernet facility. The real value of these products is the ability to simultaneously manage sensor data from different protocols (ANT/ANT+/ANT-FS/BLE). Both products can be situated in a designated monitoring area to provide complete coverage of BLE and various ANT protocol sensor data coming from groups of people or groups of devices. Portions of data initially captured by one node will continue to be captured through other nodes as the individual moves throughout the monitoring area thus creating a smart, real-time monitoring solution. WASP-N operates in WiFi infrastructure networks or as a limited Access Point (AP), where it creates its own network.

Real-time sensor information captured by WASP can be communicated to numerous end points, either directly onto a wired network using WASP-POE or to any WiFi enable device with WASP-N, such as a smart phone pad, or watch. Group information can be displayed onto a big board for the class, while individual information can be viewed by each participant via their phone.

This will act as a running change list of the NpeGate Framework.

### Contribution guidelines ###

* Document all methods.  As part of the documentation, use the since tag to provide when the method was added to the framework.
* Test.  If possible add a XCTestCase for the calls.

### TAGS ###
After Each Release Build Version TAG the Build so we have a reference code base to be able to go back to

Example:
~~~
git tag -a 2.0b1 -m '2.0 Beta1'
git push origin --tags
~~~


### Who do I talk to? ###

* Repo owner or admin

### Required For Building Framework ###

#### Version ####

- iOS Dynamic requires 9.0
- tvOS requires 9.0
- macOS requires 10.10

#### Required Frameworks for Building ####

The following frameworks are required to build with the WASP API.

- CFNetwork.framework
- SystemConfiguration.framework
- CoreFoundation.framework
- CoreBluetooth.framework
- Security.framework
- libCommonCrypto.dylib     (as of 2.0)
- libz                      (as of 2.0)
- IOKit.framework           (as of 2.0 macOS only)

#### Required Capabilities ####

The framework requires network acces information to properly discover WASPs on the network.

- Access WiFi Information
- Hotspot Configuration

#### Required Location Services ####

Apple uses location services in conjunction with the network information  discovery.
If location services are not requested there will be a persistant warning message in the app log file.
The application using the NpeGate Framework does not need to activley monitor the location, but
It needs to have the correct entitlements to ask the user if location services can be used.


<key>NSLocationWhenInUseUsageDescription</key>
<string>Application requires user’s location for better user experience.</string>

