// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NpeGate",
    products: [
        .library(name: "NpeGate", targets: ["NpeGate"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name:"NpeGate",
            path:"./NpeGate.xcframework"
        ),
    ]
)
