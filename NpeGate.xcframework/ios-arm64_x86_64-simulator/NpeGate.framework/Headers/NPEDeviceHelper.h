//
//  NPEDeviceHelper.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/npe_hardware_connector_types.h>
#import <NpeGate/NPEANT.h>
#import <NpeGate/NPESensorLocationProtocol.h>
#import <NpeGate/NPEFITBodyLocation.h>

@class NPESensorConnection;

typedef NS_ENUM(NSInteger, PairingSource) {
    PairingSourceNone,
    PairingSourcePairingPod,
    PairingSourceHeartbeatz,
    PairingSourceWasp_n,
    PairingSourceWasp_poe,
    PairingSourceGem3_poe,
    PairingSourceHornet,
    PairingSourceThreshold
};



@interface NPEDeviceHelper : NSObject

+(NSString *)PairingSourceToString:(PairingSource) value;
+(PairingSource)PairingSourceFromString:(NSString *)string;

/**
 <b>INTERNAL:</b> Gets the Frameworks Sensor Location from the ANT Common Page Sensor Location (uses FIT enum)

 @param location The FIT Body Loction Enum for Common Page Sensor Location
 @returns The Framework Sensor Location
 @warning Internal use only
 @since 3.0
 */
+ (NPESensorLocation)getSensorLocationFromANTCommonPageSensorLocation:(FITBodyLocation)location;

/**
 <b>INTERNAL:</b> Gets the Frameworks Sensor Location from the ANT Footpod Location
 
 @param location The ANT Footpod Location
 @returns The Framework Sensor Location
 @warning Internal use only
 @since 2.0
 */
+ (NSInteger)getSensorLocaitonFromANTFootpodLocation:(NSInteger)location;


/**
 <b>INTERNAL:</b> Gets the Frameworks Sensor Location from the BLE Body Location
 
 @param bleLocation The BLE Body Location
 @returns The Framework Sensor Location
 @warning Internal use only
 @since 2.0
 */
+ (NSInteger)getSensorLocaitonFromBLEBodyLocation:(NSInteger)bleLocation;

/**
 <b>INTERNAL:</b> Gets the Frameworks Sensor Location from the BLE Sensor Location
 
 Refers to the org.bluetooth.characteristic.sensor_location
 
 @param bleLocation The BLE Sensor Location
 @returns The Framework Sensor Location
 @warning Internal use only
 @since 2.0
 */
+ (NSInteger)getSensorLocaitonFromBLESensorLocation:(NSInteger)bleLocation;


/**
 <b>INTERNAL:</b> Creates the NPESensorConnection instance for the NPESensorType
 
 @param sensorType The NPESensorType
 @returns The Instances of the NPESensorConnection for the SensorType
 @warning Internal use only
 */
+ (NPESensorConnection *)createSensorConnection:(NPESensorType)sensorType;

/**
 <b>INTERNAL:</b> Gets the NPESensorType from the ANT DeviceType and TransType
 
 @param devType The ANT DeviceType
 @param transType The ANT TransType
 @returns The Instances of the NPESensorConnection for the SensorType
 @warning Internal use only
 */
+ (NPESensorType)getSensorTypeFromDeviceType:(NPEAntDeviceType)devType withTransType:(NPEAntTransType)transType;

/**
 <b>INTERNAL:</b> Gets the ANT DeviceType  from the NPESensorType and TransType

 @param devType The NPESensorType
 @param transType The ANT TransType
 @returns The NPEAntDeviceType
 @warning Internal use only
 */
+ (NPEAntDeviceType)getAntDeviceTypeFromSensorType:(NPESensorType)devType withTransType:(NPEAntTransType)transType;

@end
