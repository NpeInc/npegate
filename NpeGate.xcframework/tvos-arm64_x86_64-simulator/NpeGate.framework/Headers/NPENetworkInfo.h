//
//  NPENetworkInfo.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Network Interface
 */
@interface NPENetworkInterface : NSObject

/**
 Interface Name
 */
@property (nonatomic, readonly) NSString *name;



/**
 Interface Supports Multicast
 */
@property (nonatomic, readonly) BOOL multicastSupported;

/**
 Interface Address for IPV4
 */
@property (nonatomic, readonly, nullable) NSString *ipv4Address;

/**
 Interface Broadcast Address for IPV4
 */
@property (nonatomic, readonly, nullable) NSString *ipv4Broadcast;

/**
 Interface Netmask for IPV4
 */
@property (nonatomic, readonly, nullable) NSString *ipv4Netmask;

/**
 Interface Address for IPV6
 */
@property (nonatomic, readonly, nullable) NSString *ipv6Address;

/**
 Interface Netmask for IPV6
 */
//@property (nonatomic, readonly, nullable) NSString *ipv6Netmask;

@end


typedef NSString * NPENetworkInfoKey NPE_STRING_ENUM;

#if TARGET_OS_IPHONE
/** The key for an iOS Devices WiFi interface */
FOUNDATION_EXPORT NPENetworkInfoKey const NPENetworkInfoKeyIOSWiFiInterfacekey;
#endif

/** The key for an IPv4 IP Address */
FOUNDATION_EXPORT NPENetworkInfoKey const NPENetworkInfoKeyIPv4Addresskey;
/* The key for an IPv6 IP Address */
FOUNDATION_EXPORT NPENetworkInfoKey const NPENetworkInfoKeyIPv6Addresskey;


/**
 Methods to Help retrieve Network Interface Informaiton
 */
@interface NPENetworkInfo : NSObject

/**
 The shared instance.
 
 @returns A shared instance of NetworkInfo
 */
+ (NPENetworkInfo *)sharedInformation;

/**
 An Array of all NPENetworkInterface Objects found on the Device that
 are currently active
 */
@property (nonatomic, readonly) NSArray<NPENetworkInterface *> *publicAvailableInterfaces;

/**
 An Array of all NPENetworkInterface Objects found on the Device that
 contain a Valid IPv4 or IPv6 Address

 */
@property (nonatomic, readonly) NSArray<NPENetworkInterface *> *allInterfaces;

/**
 An NSArray which contains all of the existing network interface names.
 
 Network interfaces must meet the conditions of: UP, RUNNING, and not a LOOPBACK interface
 */
@property (nonatomic, readonly) NSArray<NSString *> *allInterfaceNames;

/**
 Provides the current SSID of the WiFi network.  If on simulator it will return
 <<Simulator>> as the SSID information does not get reported
 */
@property (nonatomic, readonly, copy) NSString *SSID;

/**
 Provides the IP Address for the primary interface.
 
 @param key The Key for the type of IP Address you wish.
 - kNetworkInformationIPv4Addresskey
 - kNetworkInformationIPv6Addresskey
 */
- (NSString * _Nullable)primaryIPAddressForAddressKey:(NPENetworkInfoKey)key;


///---------------------------------------------------------------------------------------
/// @name IPv4
///---------------------------------------------------------------------------------------

/**
 The primary IPv4 address.
 
 This will automatically determine which interface is the primary interface and return the address.
 */
@property (nonatomic, readonly, nullable) NSString *primaryIPv4Address;

/**
 Provides the IPv4 Address for the interface.

 @param interfaceName The name of the Interface.  Such as en0, pdp_ip0 or use <i>NPENetworkInfoKeyIOSWiFiInterfacekey</i> on iOS for the WiFi interface.
 @return IPV4 Address for Interface
 */
- (NSString * _Nullable)IPv4AddressForInterfaceName:(NSString *)interfaceName;

/**
 If Multicast is enabled on the primary IPv4 Address will return TRUE
 */
@property (nonatomic, readonly) BOOL primaryIPv4MulticastEnabled;

/**
 Returns the Broadcast Address for the primary IPv4 Address.  If not found returns nil.
 */
@property (nonatomic, readonly, nullable) NSString *primaryIPv4BroadcastAddress;



///---------------------------------------------------------------------------------------
/// @name IPv6
///---------------------------------------------------------------------------------------

/**
 The primary IPv6 address.
 
 This will automatically determine which interface is the primary interface and return the address.
 */
@property (nonatomic, readonly, nullable) NSString *primaryIPv6Address;


/**
 String Value for IPV6 Address of a specific interface

 @param interfaceName Interface Name
 @return String value of IPV6 Address
 */
- (NSString * _Nullable)IPv6AddressForInterfaceName:(NSString *)interfaceName;


///---------------------------------------------------------------------------------------
/// @name Class Methods
///---------------------------------------------------------------------------------------

/// Convert a String IP Address to an Integer based representation
+ (UInt32)ipAddressToInteger:(NSString *)ipAddress;

/// Converts a UInt32 ip address to string representation
+ (NSString *)ipAddressFromInteger:(UInt32)ipaddress;

-(void)resetInterfaces;

@end
NS_ASSUME_NONNULL_END
