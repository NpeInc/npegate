//
//  NPEBikeLights.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#endif

NS_ASSUME_NONNULL_BEGIN
///---------------------------------------------------------------------------------------
/// Sensor Data Parameter Keys
///
/// Supports Common Data Parameter Keys:
///     NPESensorDataParameterKeyTotalSteps
///
/// See also: NPECommonData
///---------------------------------------------------------------------------------------

FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBikeRadarSupport;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyLightType;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBatteryWarnings;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySubLights;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBeamFocus;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyLightBeam;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyLightState;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyLightIntensity;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyAutoIntensityMode;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBeamSupported;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySupportedModes;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBatteryCapacity   ;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySupportedSecLights;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBeamFocusCtrl;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyBeamIntensityCtrl;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySupportedStdModes;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySyncBrakeSupport;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyHLSupported;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyTLSupported;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySLSupported;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyLSLSupported;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRSLSupported;

typedef NSString * NPEBikeLightsModeKey NPE_STRING_ENUM;

FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeOff;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightMode81_100;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightMode61_80;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightMode41_60;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightMode21_40;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightMode0_20;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeSlowFlash;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeFastFlash;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeRandomFlash;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeAuto;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeTurnLeftsc;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeTurnLeft;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeTurnRightsc;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeTurnRight;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeHazardLights;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom48;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom49;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom50;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom51;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom52;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom53;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom54;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom55;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom56;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom57;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom58;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom59;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom60;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom61;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom62;
FOUNDATION_EXPORT NPEBikeLightsModeKey const NPEBikeLightModeCustom63;

/**
 Bike Light Type

 @since 3.2
 */
typedef NS_ENUM(NSInteger, BikeLightType) {
    /** Module is Right Side Up */
    HeadLight = 0,
    TailLight = 2,
    SignalLightConfigurable = 3,
    SignalLightLeft = 4,
    SignalLightRight = 5
};

/**
 Batter Level

 @since 3.2
 */
typedef NS_ENUM(NSInteger, BatteryLevel) {
    /** Module is Right Side Up */
    BatteryFull = 1,
    BatteryGood = 2,
    BatteryOk = 3,
    BatteryLow = 4,
    BatteryCritical = 5,
    BatteryCharging = 6,
    BatteryInvalid = 7
};


/**
 Represents the data available for the ANT+ Running Dynamics sensor.

 ANT+ sensors send data in multiple packets.  The NPEBikeLights combines the
 most commonly used of this data into a single entity.  The data represents
 the latest of each data type sent from the sensor.

 See Header Files for Sensor Data Parameter Keys

 @since 3.0.6
 */
@interface NPEBikeLightsData : NPESensorData

///**
// Individual bike light index
//
// @since 3.2
// */
@property (nonatomic, readonly) uint8_t lightIndex;
@property (nonatomic, readonly) uint8_t subLights;
@property (nonatomic, readonly) uint8_t lightState;
@property (nonatomic, readonly) BikeLightType lightType;
@property (nonatomic, readonly) uint8_t intensity;
@property (nonatomic, readonly) uint8_t numSupportedModes;
@property (nonatomic, readonly) uint16_t supportedModesBitField;
@property (nonatomic, readonly) UInt16 mainLightDeviceNumber;
@property (nonatomic, readonly) UInt8  mainLightBroadcastTransmissionType;

-(void)disconnectLight;
-(NSArray<NSString*>*)supportedModes;
-(NSArray<NSString*>*)supportedModesIndexes;

+(NSString*)bikeLightTypeString:(BikeLightType)type;
+(NSString*)bikeLightIntensityString:(uint8_t)intensity;
+(NSString*)bikeLightStateString:(uint8_t)state;
//
///**
// Determines if the sensor supports Bidirectional Channel
//
// @since 3.0
// */
//@property (nonatomic, readonly) BOOL biDirectionalChannel;
//
///**
// Filtered Instantaneous Vertical Oscillation (mm)
//
// @since 3.0
// */
//@property (nonatomic, readonly) CGFloat verticalOscillation;
//
///**
// Filtered Instantaneous Ground Contact Time (ms)
//
// @since 3.0
// */
//@property (nonatomic, readonly) NSInteger groundContactTime;
//
///**
// Filtered Instantaneous Stance Time Percentage
//
// @since 3.0
// */
//@property (nonatomic, readonly) CGFloat instantaneousStanceTime;
//
///**
// Total Step Count
//
// @since 3.0
// */
//@property (nonatomic, readonly) NSUInteger totalSteps;
//
///**
// Filtered Instantaneous Ground Contact Balance (%)
//
// @since 3.0
// */
//@property (nonatomic, readonly) CGFloat groundContactBalance;
//
///**
// Vertical Ratio (%))
//
// @since 3.0
// */
//@property (nonatomic, readonly) CGFloat verticalRatio;
//
///**
// Step Length (mm)
//
// @since 3.0
// */
//@property (nonatomic, readonly) NSInteger stepLength;


@end
NS_ASSUME_NONNULL_END
