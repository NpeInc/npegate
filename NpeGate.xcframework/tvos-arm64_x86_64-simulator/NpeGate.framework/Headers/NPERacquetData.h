//
//  NPERacquetData.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>
#import <NpeGate/NPESensorLocationProtocol.h>

@class NPECommonData;

NS_ASSUME_NONNULL_BEGIN
///---------------------------------------------------------------------------------------
/// Sensor Data Parameter Keys
///
/// Supports Common Data Parameter Keys:
///   NPESensorDataParameterKeyAccumStrides
///   NPESensorDataParameterKeyDistance
///   NPESensorDataParameterKeyCapabilities
///   NPESensorDataParameterKeySpeed
///   NPESensorDataParameterKeyCalories
///   NPESensorDataParameterKeyLocation
///   NPESensorDataParameterKeyTotalDistance
///   NPESensorDataParameterKeyRawData
///   NPESensorDataParameterKeyCadence
///
/// See also: NPECommonData
///---------------------------------------------------------------------------------------

/** Sensor Data Parameter Key for Total Strides */
//FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyTotalStrides;
///** Sensor Data Parameter Key for Virtual Speed */
//FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyVirtualSpeed;
///** Sensor Data Parameter Key for Update Latency */
//FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyUpdateLatency;
///** Sensor Data Parameter Key for Sensor Health */
//FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyDeviceHealth;


/**
 The health of the Racquet
 
 @since 2.1.0
 */
//typedef NS_ENUM(NSInteger, RacquetHealth) {
//    /** Racquet is Ok */
//    RacquetHealthOk         = 0,
//    /** Racquet has an Error */
//    RacquetHealthError      = 1,
//    /** Racquet has a warning */
//    RacquetHealthWarning    = 2,
//    /** Reserved Value */
//    RacquetHealthReserved   = 3
//};

typedef NS_ENUM(uint8_t, CûbeState) {
    CûbeStateIdle = 0,
    CûbeStateBottomSideUp = 1,
    CûbeStateLeftSideUp = 2,
    CûbeStateFrontSideUp = 3,
    CûbeStateBackSideUp = 4,
    CûbeStateRightSideUp = 5,
    CûbeStateTopSideUp = 6,
    CûbeStateUndeterminedSideUp = 7
};





/**
 * Represents the most commonly used data available from the ANT+ Stride sensor.
 *
 * ANT+ sensors send data in multiple packets.  The NPERacquetData combines the
 * most commonly used of this data into a single entity.  The data represents
 * the latest of each data type sent from the sensor.
 *
 * See Header Files for Sensor Data Parameter Keys
 */
@interface NPERacquetData : NPESensorData

/**
 * The side that is facing up.
 */
@property (nonatomic, readonly) CûbeState cûbeStatePrimary;

/**
 * If the Cûbe stops on an edge the secondary side is the other side facing up
 * In this condition the app can indicate an edge result.
 */
@property (nonatomic, readonly) CûbeState cûbeStateSecondary;

/**
 * Set to a one when the Cûbe has a stable valid measurement
 */
@property (nonatomic, readonly) BOOL      cûbeStateSettled;


@property (nonatomic, readonly) float xForce;
@property (nonatomic, readonly) float yForce;
@property (nonatomic, readonly) float zForce;
@property (nonatomic, readonly) float yaw;
@property (nonatomic, readonly) float pitch;
@property (nonatomic, readonly) float roll;

/// User facing String for the CûbeState side indicator.
/// @param state CûbeState
+ (NSString *_Nonnull)cûbeStateString:(CûbeState)state;

@end
NS_ASSUME_NONNULL_END
