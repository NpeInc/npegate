//
//  NPESensorCmdParams.h
//  NpeGate
//
//  Created by Rick Gibbs on 6/16/21.
//  Copyright © 2021 North Pole Engineering. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NPESensorCmdParams : NSObject

@property (nonatomic, nonnull, strong) NSString *name;
@property (nonatomic, nonnull, strong) NSString *type;
@property (nonatomic, nonnull, strong) NSNumber *setting;
@property (nonatomic, nonnull, strong) NSNumber *stepBy;
//@property (nonatomic, nonnull, strong) NSString *description;
@property (nonatomic, nullable, strong) NSString *units;
@property (nonatomic, nullable, strong) NSArray<NSString*> *pickerRowTitles;
@property (nonatomic, nullable, strong) NSArray<NSNumber*> *pickerRowValues;
@property (nonatomic, nullable, strong) NSArray<NSString*> *segmentTitles;
@property (nonatomic, nullable, strong) NSArray<NSNumber*> *segmentValues;
@property (nonatomic, nullable, strong) NSNumber *max;
@property (nonatomic, nullable, strong) NSNumber *min;
@property (nonatomic, nullable, strong) NSNumber *numberOfBytes;
@property (nonatomic, nullable, strong) NSNumber *byteOrder;
@property (nonatomic, nullable, strong) NSNumber *offset;
@property (nonatomic, nullable, strong) NSArray *antPkt;


-(instancetype)initPicker:(NSString*)name description:(NSString*)description withTitles:(NSArray<NSString*>*)titles andValues:(NSArray<NSNumber*>*)values initialIndex:(NSNumber*)index;

-(instancetype)initSwitch:(NSString*)name description:(NSString*)description initialSetting:(NSNumber*)isOn;

@end

NS_ASSUME_NONNULL_END
