//
//  NPECoreData.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#endif

@class NPECommonData;


///---------------------------------------------------------------------------------------
/// Sensor Data Parameter Keys
///
/// Supports Common Data Parameter Keys:
///   NPESensorDataParameterKeyCurrentTemp
///   NPESensorDataParameterKeyRawData
///   NPESensorDataParameterKeyCurrentPressure
///   NPESensorDataParameterKeyCurrentHumidity
///
/// See also: NPECommonData
///---------------------------------------------------------------------------------------


/** Defines the Sensor Data Quality */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyCoreDataQuality;
/** Defines the Sensor Data Parameter Key for Skin Temperature */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeySkinTemperature;
/** Defines the Sensor Data Parameter Key for Core Temperature */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyCoreTemperature;
/** Defines the Sensor Data Parameter Key for Core Hr Supported */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyHrSupported;
/** Defines the Sensor Data Parameter Key for Core Local Time Supported */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyLocalTimeSupported;
/** Defines the Sensor Data Parameter Key for Core UTC TimeSupported */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyUtcTimeSupported;
/** Defines the Sensor Data Parameter Key for Core Default Transmission Rate */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyDefaultTransRate;
/** Defines the Sensor Data Parameter Key for Core Reserved Field */
FOUNDATION_EXPORT NPESensorDataParameterKey _Nonnull const NPESensorDataParameterKeyCoreReserved;


/**
 * Describes the status of the time support in the ANT sensor.
 */
typedef NS_ENUM(NSInteger, NPECoreFeature) {
    /** No time information is available. */
    NPECoreFeatureNotSupported              = 0,
    /** The time is supported but not set. */
    NPECoreFeatureSupportNotSet             = 1,
    /** The  time is supported but not set. */
    NPECoreFeatureSupportAndSet             = 2,
};


/**
 * Describes the status of the time support in the ANT sensor.
 */
typedef NS_ENUM(NSInteger, NPECoreDataQuality) {
    /** CORE measurement Data Quality Poor. */
    NPECoreDataQualityPoor              = 0,
    /** CORE measurement Data Quality Fair. */
    NPECoreDataQualityFair              = 1,
    /** CORE measurement Data Quality Good. */
    NPECoreDataQualityGood              = 2,
    /** CORE measurement Data Quality Excellent. */
    NPECoreDataQualityExcellent         = 3,
    /** CORE measurement Data Quality Excellent. */
    NPECoreDataQualityNotUsed           = 0xFF,

};


/**
 * Represents the data available from the ANT+ Core sensor.
 *
 * ANT+ sensors send data in multiple packets.  The NPECoreData
 * combines this data into a single entity.  The data represents the latest of
 * each data type sent from the sensor.
 *
 * See Header Files for Sensor Data Parameter Keys
 */
@interface NPECoreData : NPESensorData

/**
 * CORE Data Qaulity.
 *
 * @note The four valid results are
 * 0 - Poor
 * 1 - Fair
 * 2 - Good
 * 3 - Excellent
 */@property (nonatomic, readonly) NPECoreDataQuality dataQuality;

/**
 * Heart Rate Support.
 *
 * @note The three valid results are
 * 0 - Heart Rate Not Supported
 * 1 - Heart Rate Supported But Not Set
 * 2 - Heart Rate  Supported And Set
 */
@property (nonatomic, readonly) NPECoreFeature hrSupport;

/**
 * Local Time Support.
 *
 * @note The three valid results are
 * 0 - Local Time Not Supported
 * 1 - Local Time Supported But Not Set
 * 2 - Local Time Supported And Set
 */
@property (nonatomic, readonly) NPECoreFeature localTimeSupport;

/**
 * UTC Time Support.
 *
 * @note The three valid results are 
 * 0 - UTC Time Not Supported
 * 1 - UTC Time Supported But Not Set
 * 2 - UTC Time Supported And Set
 */
@property (nonatomic, readonly) NPECoreFeature utcTimeSupport;

/**
 * Supported Pages.
 *
 * @note Each Bit represents a supported page 0-31 
 */
@property (nonatomic, readonly) NSUInteger supportedPages;

/**
 * Event counter increments with each information update.
 */
@property (nonatomic, readonly) u_char eventCount;

/**
 * Default Tx Rate.
 *
 * @note The two valid results are
 * 0 - 0.5 Hz
 * 1 - 4 Hz
 */
@property (nonatomic, readonly) float defaultTxRate;

///---------------------------------------------------------------------------------------
/// @name Temperature Information
///---------------------------------------------------------------------------------------

/**
 *  Current Temperature in (NPEUnitTypeCelsius)
 *
 *  Signed CGFloat representing the current skin temperature
 *  -204.7 - 204.7C
 */
@property (nonatomic, readonly) CGFloat skinTemp;

/**
 *  Current CORE Temperature in (NPEUnitTypeCelsius)
 *
 *  Signed CGFloat representing the current CORE temperature
 *  -327.67 - 327.67
 */
@property (nonatomic, readonly) CGFloat coreTemp;

//-(NSString * _Nonnull)stringForDataQuality;

@end
