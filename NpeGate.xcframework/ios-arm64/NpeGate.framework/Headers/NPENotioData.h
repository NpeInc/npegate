//
//  NPENotioData.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#endif

NS_ASSUME_NONNULL_BEGIN
///---------------------------------------------------------------------------------------
/// Sensor Data Parameter Keys
///
/// Supports Common Data Parameter Keys:
///     NPESensorDataParameterKeyTotalSteps
///
/// See also: NPECommonData
///---------------------------------------------------------------------------------------
/** Defines the Sensor Data Parameter Key Ecrr*/
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyEcrr;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyEacc;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyEcda;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyEalt;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyEtot;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyAntLap;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRiderFactor;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyStickFactor;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyCrr;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyTimeWindow;
/**
 Module Orientation

 @since 3.0
 */
//typedef NS_ENUM(NSInteger, NotioModuleOrientation) {
//    /** Module is Right Side Up */
//    NotioModuleOrientationRightSideUp = 0,
//    /** Module is Upside Down */
//    NotioModuleOrientationUpsideDown = 1,
//};
//
//typedef NS_ENUM(uint8_t, NotioThreatLevel) {
//    NoThreat = 0,
//    Approach = 1,
//    FastApproach = 2,
//    ReservedLevel = 3
//};
//
//typedef NS_ENUM(uint8_t, NotioThreatSide) {
//    NoSide = 0,
//    Right = 1,
//    Left = 2,
//    ReservedSide = 3
//};
//
//
//
//typedef struct {
//    NotioThreatLevel threatLevel;
//    NotioThreatSide  threatSide;
//    double           targetRange;
//    double           approachSpeed;
//} NotioTarget_t;

@interface NPENotioData : NPESensorData

/**
 Determinss if the user is Running or Walking

 @since 3.0
 */
@property (nonatomic, readonly) uint8_t status;
@property (nonatomic, readonly) BOOL hrm;
@property (nonatomic, readonly) BOOL dfly;
@property (nonatomic, readonly) BOOL speed_cadence;
@property (nonatomic, readonly) BOOL cadence;
@property (nonatomic, readonly) BOOL speed;
@property (nonatomic, readonly) BOOL pwr;
@property (nonatomic, readonly) BOOL garmin_pause;
@property (nonatomic, readonly) BOOL recording;
@property (nonatomic, readonly) float windSpeed;
@property (nonatomic, readonly) float ecrr;
@property (nonatomic, readonly) float eacc;
@property (nonatomic, readonly) uint8_t lap;
@property (nonatomic, readonly) float ecda;
@property (nonatomic, readonly) float ealt;
@property (nonatomic, readonly) float etot;

@end
NS_ASSUME_NONNULL_END
