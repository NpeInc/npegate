//
//  NPESimGen.h
//  WaspSimulator
//
//  Created by RickGibbs on 2/25/15.
//  Copyright (c) 2015 NPE Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPEANTDefines.h>
#import <NpeGate/npe_hardware_connector_types.h>
//#import <NpeGate/NpeGate.h>

#define kGen200SimData @"kGen200SimData"
#define kGen1024SimData @"kGen1024SimData"
#define kGenSimData @"kGenSimData"
#define kGenAntData @"kGenAntData"
#define kGenRespData @"kGenRespData"

typedef struct{
    uint8_t length;
    uint8_t cmd;
    uint8_t chan;
    uint8_t data[8];
    uint8_t flag;
    uint16_t devID;
    uint8_t devType;
    uint8_t transType;
    uint8_t threshType;
    uint8_t threshLevel;
    uint8_t thresh;
} antPkt_t;

typedef struct __attribute__((packed)) {
    uint8_t  length;
    uint8_t  cmd;
    int8_t   rssi;
    uint32_t serialNumber;
    uint8_t  msgId;
    union {
        struct __attribute__((packed)) {
            uint8_t flags;
            uint16_t instantHr;
            uint8_t data[14];
        } fitnessData;
        struct __attribute__((packed)) {
            uint8_t devMsgId;
            union {
                struct __attribute__((packed)) {
                    uint8_t state;
                    uint8_t activeTime[3];
                    uint8_t pauseTime[3];
                    uint16_t workoutType;
                    uint8_t reserved[7];
                } sessionInfo;
                uint8_t data[16];
            } hbzDevicePayload;
        } deviceMsg;
        uint8_t data[17];
    } hbzPayload;
} heartbeatzPkt_t;

typedef struct __attribute__((packed)) {
    uint8_t  payloadLength;
    uint8_t  manufacturerCommand;
    uint8_t  radioSubcommand;
    uint8_t  reserved;
    uint32_t serialNumber;
    uint8_t  data[32];
    uint8_t  reserved2[3];
} heartbeatzSendPkt_t;

typedef NS_ENUM(uint8_t, AweCommand_t) {
    ANT_HRM_127_COMMAND_INVALID                 = 0,
    ANT_HRM_127_COMMAND_ANT_OFF                 = 1,
    ANT_HRM_127_COMMAND_CONTROL_STATUS          = 2,
    ANT_HRM_127_COMMAND_USER_ALERT              = 3,
    ANT_HRM_127_COMMAND_USER_REQUEST            = 4,
    ANT_HRM_127_COMMAND_CALORIES                = 5,
    ANT_HRM_127_COMMAND_POINTS                  = 6,
    ANT_HRM_127_COMMAND_HR_THRESHOLD            = 7,
    ANT_HRM_127_COMMAND_TOTAL_STRIDES           = 8,
    ANT_HRM_127_COMMAND_TOTAL_DISTANCE          = 9,
    ANT_HRM_127_COMMAND_INSTANT_SPEED           = 10,
    ANT_HRM_127_COMMAND_INSTANT_STRIDES_PER_MIN = 11,
    ANT_HRM_127_RESERVED                        = 12,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_1          = 13,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_2          = 14,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_3          = 15,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_4          = 16,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_5          = 17,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_6          = 18,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_7          = 19,
    ANT_HRM_127_COMMAND_TIME_IN_ZONE_8          = 20,
    ANT_HRM_127_COMMAND_HEART_RATE_AVERAGE      = 21,

};




@protocol NPESimGenDelegate

-(BOOL)genSimDataForType:(NPESensorType)type withNum:(uint32_t)num;
-(BOOL)checkProxForType:(NPESensorType)type withNum:(uint32_t)num;
-(BOOL)checkZeroForType:(NPESensorType)type withNum:(uint32_t)num;
-(uint32_t)getDeviceOffset;

@end

@interface NPESimParams: NSObject

@property(nonatomic, assign) BOOL enabled;
@property(nonatomic, assign) BOOL accumulated;
@property(nonatomic, assign) BOOL bRamp;
@property(nonatomic, strong) NSNumber *value;
//@property(nonatomic, strong) NSNumber * rampData;
@property(nonatomic, strong) NSNumber * prevData;
@property(nonatomic, strong) NSNumber * rampDelta;
@property(nonatomic, strong) NSNumber * max;
@property(nonatomic, strong) NSNumber * min;
@property(nonatomic, strong) NSDate * prevDate;
@property(nonatomic, strong) NSNumber * scaling;
@property(nonatomic, strong) NPESimParams *source;
@property(nonatomic, strong) NSNumber * lastEventTime;
@property(nonatomic, strong) NSNumber * eventTime;
@property(nonatomic, strong) NSNumber * eventCnt;
@property(nonatomic, strong) NSNumber * cnt;
@property(nonatomic, strong) NSString * name;

@end



@interface NPESimGen : NSObject

@property (nonatomic) id <NPESimGenDelegate> delegate;
@property (nonatomic, assign) NSInteger envDevNum;
@property (nonatomic, assign) NSInteger hbDevNum;
@property (nonatomic, assign) NSInteger hrDevNum;
@property (nonatomic, assign) NSInteger aweDevNum;
@property (nonatomic, assign) NSInteger pwrDevNum;
@property (nonatomic, assign) NSInteger fitDevNum;
@property (nonatomic, assign) NSInteger treadDevNum;
@property (nonatomic, assign) NSInteger rowerDevNum;
@property (nonatomic, assign) NSInteger bikeDevNum;
@property (nonatomic, assign) NSInteger stBikeDevNum;
@property (nonatomic, assign) NSInteger cadDevNum;
@property (nonatomic, assign) NSInteger spdDevNum;
@property (nonatomic, assign) NSInteger sdmDevNum;
@property (nonatomic, assign) NSInteger diagDevNum;
@property (nonatomic, assign) uint32_t serialNumber;
@property (nonatomic, readonly) uint32_t longId;

@property (nonatomic, strong) NPESimParams * hr;
@property (nonatomic, strong) NPESimParams * pwr;
@property (nonatomic, strong) NPESimParams * pwrCad;
@property (nonatomic, strong) NPESimParams * env;
@property (nonatomic, strong) NPESimParams * cad;
@property (nonatomic, strong) NPESimParams * spd;
@property (nonatomic, strong) NPESimParams * sdm;
@property (nonatomic, strong) NPESimParams * fit;
@property (nonatomic, strong) NPESimParams * fitHr;
@property (nonatomic, strong) NPESimParams * fitPwr;
@property (nonatomic, strong) NPESimParams * fitCad;
@property (nonatomic, strong) NPESimParams * fitSpd;
@property (nonatomic, strong) NPESimParams * fitSt;
@property (nonatomic, strong) NPESimParams * fitDist;
@property (nonatomic, strong) NPESimParams * fitPosVert;
@property (nonatomic, strong) NPESimParams * fitNegVert;
@property (nonatomic, strong) NPESimParams * fitMets;
@property (nonatomic, strong) NPESimParams * fitCalBurnRate;
@property (nonatomic, strong) NPESimParams * fitCalories;
@property (nonatomic, strong) NPESimParams * fitCycleLength;
@property (nonatomic, strong) NPESimParams * fitIncline;
@property (nonatomic, strong) NPESimParams * fitResistanceLevel;


@property (nonatomic, readonly) uint16_t hrAweCalories;
@property (nonatomic, readonly) uint16_t hrAwePoints;
@property (nonatomic, readonly) uint16_t hrAweWorkoutState;

@property (readonly, nonatomic) float hrValue;
@property (readonly, nonatomic) float pwrValue;
@property (readonly, nonatomic) float pwrcadValue;
@property (readonly, nonatomic) float cadValue;
@property (readonly, nonatomic) float spdValue;
@property (readonly, nonatomic) float envValue;
@property (readonly, nonatomic) float sdmValue;
@property (readonly, nonatomic) float sdmStrideValue;


-(void)antMsg:(NSData*)msg forType:(NPEAntDeviceType)devType;
-(void)heartbeatzMsg:(NSData*)msg;

@end
