//
//  NPERadarData.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#elif TARGET_OS_MAC
#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#endif

NS_ASSUME_NONNULL_BEGIN
///---------------------------------------------------------------------------------------
/// Sensor Data Parameter Keys
///
/// Supports Common Data Parameter Keys:
///     NPESensorDataParameterKeyTotalSteps
///
/// See also: NPECommonData
///---------------------------------------------------------------------------------------
/** Defines the Sensor Data Parameter Key Running */
//FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRunning;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_1;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_2;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_3;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_4;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_5;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_6;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_7;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarApproachSpeed_8;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_1;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_2;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_3;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_4;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_5;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_6;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_7;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarRange_8;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_1;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_2;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_3;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_4;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_5;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_6;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_7;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyRadarThreat_8;

/**
 Module Orientation

 @since 3.0
 */
typedef NS_ENUM(NSInteger, RadarModuleOrientation) {
    /** Module is Right Side Up */
    RadarModuleOrientationRightSideUp = 0,
    /** Module is Upside Down */
    RadarModuleOrientationUpsideDown = 1,
};

typedef NS_ENUM(uint8_t, RadarThreatLevel) {
    NoThreat = 0,
    Approach = 1,
    FastApproach = 2,
    ReservedLevel = 3
};

typedef NS_ENUM(uint8_t, RadarThreatSide) {
    NoSide = 0,
    Right = 1,
    Left = 2,
    ReservedSide = 3
};



typedef struct {
    RadarThreatLevel threatLevel;
    RadarThreatSide  threatSide;
    double           targetRange;
    double           approachSpeed;
} RadarTarget_t;

@interface NPERadarTarget: NSObject

/**
 Represents the data available for the ANT+ Bike Radar sensor.

 ANT+ sensors send data in multiple packets.  The NPERadarData combines the
 most commonly used of this data into a single entity.  The data represents
 the latest of each data type sent from the sensor.

 See Header Files for Sensor Data Parameter Keys
 @since 3.2
 */

@property (nonatomic, assign) RadarThreatLevel threatLevel;
@property (nonatomic, assign) RadarThreatSide  threatSide;
@property (nonatomic, strong) NSNumber         *targetRange;
@property (nonatomic, strong) NSNumber         *approachSpeed;

@end


@interface NPERadarData : NPESensorData

/**
 Determinss if the user is Running or Walking

 @since 3.0
 */
@property (nonatomic, readonly) NSArray<NPERadarTarget*> *targets;

@property (nonatomic, readonly) uint8_t deviceState;
@property (nonatomic, readonly) uint8_t deviceCmd;

@end
NS_ASSUME_NONNULL_END
