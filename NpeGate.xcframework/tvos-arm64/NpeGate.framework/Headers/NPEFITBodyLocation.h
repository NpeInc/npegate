//
//  NPEFITBodyLocation.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FITBodyLocation) {
    /** Left Leg */
    FITBodyLocationLeftLeg                  = 0,
    /** Left Calf */
    FITBodyLocationLeftCalf                 = 1,
    /** Left Shin */
    FITBodyLocationLeftShin                 = 2,
    /** Left Hamstring */
    FITBodyLocationLeftHamstring            = 3,
    /** Left Quad */
    FITBodyLocationLeftQuad                 = 4,
    /** Left Glute */
    FITBodyLocationLeftGlut                 = 5,
    /** Right Leg */
    FITBodyLocationRightLeg                 = 6,
    /** Right Calf */
    FITBodyLocationRightCalf                = 7,
    /** Right Shin */
    FITBodyLocationRightShin                = 8,
    /** Right Hamstring */
    FITBodyLocationRightHamstring           = 9,
    /** Right Quad */
    FITBodyLocationRightQuad                = 10,
    /** Right Glute */
    FITBodyLocationRightGlute               = 11,
    /** Torso Back */
    FITBodyLocationTorsoBack                = 12,
    /** Left Lower Back */
    FITBodyLocationLeftLowerBack            = 13,
    /** Left Upper Back */
    FITBodyLocationLeftUpperBack            = 14,
    /** Right Lower Back */
    FITBodyLocationRightLowerBack           = 15,
    /** Right Upper Back */
    FITBodyLocationRightUpperBack           = 16,
    /** Torso Front */
    FITBodyLocationTorsoFront               = 17,
    /** Left Abdomen */
    FITBodyLocationLeftAbdomen              = 18,
    /** Left Chest */
    FITBodyLocationLeftChest                = 19,
    /** Right Abdomen */
    FITBodyLocationRightAbdomen             = 20,
    /** Right Chest */
    FITBodyLocationRightChest               = 21,
    /** Left Arm */
    FITBodyLocationLeftArm                  = 22,
    /** Left Shoulder */
    FITBodyLocationLeftShoulder             = 23,
    /** Left Bicep */
    FITBodyLocationLeftBicep                = 24,
    /** Left Tricep */
    FITBodyLocationLeftTricep               = 25,
    /** Left Brachioradialis - Left Anterior Forearm */
    FITBodyLocationLeftBrachioradialis      = 26,
    /** Left Forearm Extensors - Left Posterior Forearm */
    FITBodyLocationLeftForearmExtensors     = 27,
    /** Right Arm */
    FITBodyLocationRightArm                 = 28,
    /** Right Shoulder */
    FITBodyLocationRightShoulder            = 29,
    /** Right Bicep */
    FITBodyLocationRightBicep               = 30,
    /** Right Tricep */
    FITBodyLocationRightTricep              = 31,
    /** Right Brachioradialis - Right Anterior Forearm */
    FITBodyLocationRightBrachioradialis     = 32,
    /** Right Forearm Extensors - Right Posterior Forearm */
    FITBodyLocationRightForearmExtensors    = 33,
    /** Neck */
    FITBodyLocationNeck                     = 34,
    /** Throat */
    FITBodyLocationThroat                   = 35,
    /** Waist Mid Back */
    FITBodyLocationWaistMidBack             = 36,
    /** Waist Front */
    FITBodyLocationWaistFront               = 37,
    /** Waist Left */
    FITBodyLocationWaistLeft                = 38,
    /** Waist Right */
    FITBodyLocationWaistRight               = 39
};

/**
 ANT FIT Body Location
 */
@interface NPEFITBodyLocation : NSObject

@end
