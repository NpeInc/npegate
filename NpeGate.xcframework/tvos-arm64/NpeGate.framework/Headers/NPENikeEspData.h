//
//  NPENikeEspData.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorData.h>

@class NPEBLEAdvertisement;


NS_ASSUME_NONNULL_BEGIN
/////---------------------------------------------------------------------------------------
///// Sensor Data Parameter Keys
/////
///// Supports Common Data Parameter Keys:
/////   NPESensorDataParameterKeyRawData
/////
///// See also: NPECommonData
/////---------------------------------------------------------------------------------------
///** Defines the Sensor Data Parameter Key for Nike Fuel Rate From a sensor */
//FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyFuelRate;
///** Defines the Sensor Data Parameter Key for Nike Session Fuel From a sensor */
//FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySessionFuel;
///** Defines the Sensor Data Parameter Key for Nike Session Work From a sensor */
//FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeySessionWork;
///** Defines the Sensor Data Parameter Key for Nike Heart Rate Strain From a sensor */
//FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyHeartRateStrain;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeSequence;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeFsr0;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeFsr1;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeFsr2;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeFsr3;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeAccel0;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeAccel1;
FOUNDATION_EXPORT NPESensorDataParameterKey const NPESensorDataParameterKeyNikeAccel2;


typedef NS_ENUM(uint8_t, EspPosition) {
    EspUnknown,
    EspUnused,
    EspRight,
    EspLeft,
};


/**
 Represents the data available from a Nike ESP Sensor

 @since 3.0.8
 */
@interface NPENikeEspData : NPESensorData

/**
 If the Sensor is in association mode

 @since 3.0.8
 */
@property (nonatomic, readonly) BOOL associationMode;

/**
 If the Sensor is charging
 
 @since 3.0.8
 */
@property (nonatomic, readonly) BOOL isCharging;

/**
 If the Sensor is in association mode
 
 @since 3.0.8
 */
@property (nonatomic, readonly) EspPosition position;

/**
 Version Major
 
 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSNumber *versionMajor;

/**
 Version Minor
 
 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSNumber *versionMinor;

/**
 Elapsed Time

 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSArray<NSNumber*> *nikeID;

/**
 If the Sensor is in error
 
 @since 3.0.8
 */
@property (nonatomic, readonly) BOOL error;

/**
 Current Power

 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSNumber *sequence;

/**
 Nike Fuel Rate

 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSArray<NSNumber*> *fsrData;

/**
 Nike Fuel Rate

 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSArray<NSNumber*> *owmemData;

/**
 Nike Fuel Rate

 @since 3.0.8
 */
@property (nonatomic, nonnull, readonly) NSArray<NSNumber*> *accelData;


@end
NS_ASSUME_NONNULL_END
