//
//  NPEBikeLightsConnection.h
//  NpeGate
//
//  NPEGATE Copyright © 2012-2018 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import <Foundation/Foundation.h>
#import <NpeGate/NPESensorConnection.h>

FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyConnectLight;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyDisconnectLight;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyRequestModeDescription;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyRequestSublightModeSupport;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyRequestConnectedLightInfo;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeyRequestMainChannelID;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeySetLightMode;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeySetBeamFocus;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeySetBeamIntensity;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeySetBeamFocusIntensity;
FOUNDATION_EXPORT NPESensorCmdKey _Nonnull const NPESensorCmdKeySetBrakeEvent;

///----------------------------------------------------------
/// Light Command Parameter Keys
///----------------------------------------------------------

FOUNDATION_EXPORT NPESensorCommandParameterKey _Nonnull const NPESensorCommandParameterKeyLightMode;
FOUNDATION_EXPORT NPESensorCommandParameterKey _Nonnull const NPESensorCommandParameterKeyLightType;
FOUNDATION_EXPORT NPESensorCommandParameterKey _Nonnull const NPESensorCommandParameterKeyBeamFocus;
FOUNDATION_EXPORT NPESensorCommandParameterKey _Nonnull const NPESensorCommandParameterKeyBeamIntensity;
FOUNDATION_EXPORT NPESensorCommandParameterKey _Nonnull const NPESensorCommandParameterKeyBrakeCtrl;


NS_ASSUME_NONNULL_BEGIN
///---------------------------------------------------------------------------------------
/// Sensor Command Keys
///
/// Supports Common Commands:
///     NPESensorCmdKeyRequestManufacturerInfo
///     NPESensorCmdKeyRequestVersionInfo
///     NPESensorCmdKeyRequestBatteryStatus
///---------------------------------------------------------------------------------------

/**
 Represents a connection to Bike Light

 @since 3.2
 */
@interface NPEBikeLightsConnection : NPESensorConnection

@property (nonatomic, assign)    uint8_t commandSequenceNumber;

-(void)addSupportedLightModes;

@end
NS_ASSUME_NONNULL_END
