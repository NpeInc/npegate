//
//  WaspTableViewController.swift
//  ShowSensorsSwift
//
//  NPEGATE SAMPLE Copyright © 2012-2016 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  Rick Gibbs
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

import UIKit
import CoreLocation

@available(iOS 10.0, *)
class WaspTableViewController: UITableViewController,CLLocationManagerDelegate {
    var manager:CLLocationManager = CLLocationManager()
    var allowNetworkCheck = true
    var checkNetworkAlert = UIAlertController(title: "LOCAL NETWORK NOT ACCESSIBLE", message: "Check the Local Network Settings to ensure it is enabled.  The Local Network must be enabled for WASP traffic to be accessible to the App.", preferredStyle: .alert)

    
    override func viewDidLoad() {
        super.viewDidLoad()

        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.requestAlwaysAuthorization()
        
        checkNetworkAlert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { action in
        }))
        checkNetworkAlert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
            self.appSettings()
            self.perform(#selector(self.delayedClear), with: nil, afterDelay: 2.0)
        }))

//        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 5.5, repeats: true) { timer in
                self.tableView.reloadData()
            }
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                self.updateNetworkDiagnostics()
            }
//        } else {
//            // Fallback on earlier versions
//            Timer.scheduledTimer(timeInterval: 5.5, target: self, selector: #selector(updateWaspDisplay), userInfo: nil, repeats: true)
//            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateNetworkDiagnostics), userInfo: nil, repeats: true)
//
//        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.tableView.reloadData()
    }
    
    @objc func delayedClear()
    {
        allowNetworkCheck = true
    }

    
    func appSettings()
    {
        let application = UIApplication.shared
        let URL =  NSURL(fileURLWithPath: UIApplication.openSettingsURLString)
        if application.responds(to: #selector(UIApplication.open(_:options:completionHandler:)))
        {
            application.open(URL as URL, options: [:]) { succcess in
                if succcess == true
                {
                    print("Opened url:\(UIApplication.openSettingsURLString)")
                }
            }
        } else {
            application.open(URL as URL) { success in
                print("Open \(UIApplication.openSettingsURLString): \(success)");
            }
        }
    }

    @objc func updateWaspDisplay()
    {
        self.tableView.reloadData()
    }
    
    @objc func updateNetworkDiagnostics()
    {
        if UIApplication.shared.applicationState == .active &&
            allowNetworkCheck == true  &&
            checkNetworkAlert.isBeingPresented == false &&
            NPEHardwareConnector.shared().networkLatency?.doubleValue ?? 0 > 5000
        {
            allowNetworkCheck = false
            self.present(checkNetworkAlert, animated: true) {
                
            }
        }
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        manager.stopUpdatingLocation()
        // do something with the error
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let locationObj = locations.last {
            if locationObj.horizontalAccuracy < 10 {
                manager.stopUpdatingLocation()
                // report location somewhere else
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func myWasps() -> [Wasp] {
        return Array(NPEHardwareConnector.shared().knownWasps)
    }

}

//MARK: - UITableViewDataSource
extension WaspTableViewController {
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myWasps().count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "waspCell", for: indexPath)

        let waspObject = myWasps()[indexPath.row]

        // Configure the cell...
        cell.textLabel?.text = waspObject.name
        cell.detailTextLabel?.text = waspObject.ipAddress

        return cell
    }
    

}
