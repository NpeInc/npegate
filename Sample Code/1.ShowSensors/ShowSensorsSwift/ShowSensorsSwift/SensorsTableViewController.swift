//
//  SensorsTableViewController.swift
//  ShowSensorsSwift
//
//  NPEGATE SAMPLE Copyright © 2012-2016 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  Rick Gibbs
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

import UIKit

class SensorsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .restricted,.denied,.notDetermined:
            // report error, do something
            print("error")
        default:
            // location si allowed, start monitoring
            manager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: kNewSensorFound), object: nil, queue: nil) { (notification) -> Void in
//            self.tableView.reloadData()
//        }
//
        // OR
        GatewayManager.shared()?.gatewayConection.addSensorStateChangeObserver(self, onStateChange: { (connection, state) in
            self.tableView.reloadData()

            switch state {
            case .unknown:
                print("\(connection) in unknown state")
            case .connected:
                print("\(connection) in connected state")
            case .disconnecting:
                print("\(connection) in disconnecting state")
            default:
                print("\(connection) in undefined state")
            }
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        GatewayManager.shared()?.gatewayConection.removeSensorStateChangeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SensorsTableViewController {

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return  GatewayManager.shared().sensorSets.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let senSet:SensorSet = GatewayManager.shared().sensorSets[section]

        return senSet.sensors.count

    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let senSet:SensorSet = GatewayManager.shared().sensorSets[section]

        return senSet.name
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sensorCellReuse", for: indexPath) as! SensorCell

        // Configure the cell...

        let senSet:SensorSet = GatewayManager.shared().sensorSets[indexPath.section]
        let sensor: NPESensorConnection = senSet.sensors[indexPath.row] as! NPESensorConnection

        cell.sensor = sensor

        return cell
    }

}
