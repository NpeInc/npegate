//
//  SensorCell.swift
//  ShowSensorsSwift
//
//  NPEGATE SAMPLE Copyright © 2012-2016 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  Rick Gibbs
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

import UIKit

class SensorCell: UITableViewCell {

    @IBOutlet weak var sensorIDLabel: UILabel!
    @IBOutlet weak var sensorPPSLabel: UILabel!
    @IBOutlet weak var sensorRSSILabel: UILabel!
    @IBOutlet weak var valueOneLabel: UILabel!
    @IBOutlet weak var valueTwoLabel: UILabel!
    
    var sensor: NPESensorConnection? {
        willSet {
            
            self.sensor = newValue

            self.sensorIDLabel.text = newValue?.deviceIDString
            self.sensorPPSLabel.text = "0.0 pps"
            self.sensorRSSILabel.text = "0 dB"

            self.valueOneLabel.isHidden = true
            self.valueTwoLabel.isHidden = true
            
            self.sensor?.addSensorDataObserver(self, block: { (data) -> Void in
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.sensorPPSLabel.text = String(format: "%2.2f pps", (data.connection.pktPerSec))
                    self.sensorRSSILabel.text = String(format: "%d dB", (data.connection.rssi))

                    self.valueOneLabel.isHidden = true
                    self.valueTwoLabel.isHidden = true

                    if self.sensor is NPEHeartrateConnection {

                        //You can also look at the data.parameterList dictionary to get the values
                        if let hr = data.getParameterValue(forKey: NPESensorDataParameterKey.heartRate.rawValue, onlyIfValid: true) as? Int {

                            self.valueOneLabel.isHidden = false
                            self.valueOneLabel.text = "HR: " + String(hr)
                        }

                    } else if self.sensor is NPEBikeSpeedConnection {

                        let spd = data.getParameterValue(forKey: NPESensorDataParameterKey.speed.rawValue, onlyIfValid: true)
                        if  spd != nil {
                            self.valueOneLabel.isHidden = false
                            self.valueOneLabel.text = "Spd: " + String((spd! as AnyObject).floatValue)
                        }

                        if let val = data.getParameterValue(forKey: NPESensorDataParameterKey.wheelRRM.rawValue, onlyIfValid: true) as? Int {
                            self.valueTwoLabel.isHidden = false
                            self.valueTwoLabel.text = "RPM: " + String(val)
                        }


                    } else if self.sensor is NPEBikeCadenceConnection {

                        if let val = data.getParameterValue(forKey: NPESensorDataParameterKey.cadence.rawValue, onlyIfValid: true) {
                            self.valueOneLabel.isHidden = false
                            self.valueOneLabel.text = "CAD: " + String(describing: val)
                        }

                    } else if self.sensor is NPEBikePowerConnection {

                        if let val = data.getParameterValue(forKey: NPESensorDataParameterKey.instantPower.rawValue, onlyIfValid: true) {
                            self.valueOneLabel.isHidden = false
                            self.valueOneLabel.text = "POW: " + String(describing: val)
                        }

                    } else if self.sensor is NPEEnvironmentalConnection {

                        if let envData = self.sensor?.data as? NPEEnvironmentalData {
                            self.valueOneLabel.isHidden = false
                            self.valueOneLabel.text = String(format: "%0.2f °C", envData.currentTemp)
                        }
                    }


                })

            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        sensor?.removeSensorDataObserver(self)
    }

}
