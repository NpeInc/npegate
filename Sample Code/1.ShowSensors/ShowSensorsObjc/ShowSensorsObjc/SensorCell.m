//
//  SensorCell.m
//  ShowSensorsObjc
//
//  NPEGATE SAMPLE Copyright © 2012-2016 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  Joe Tretter
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import "SensorCell.h"
#import <NpeGate/NpeGate.h>

@interface SensorCell ()

@property (weak, nonatomic) IBOutlet UILabel *sensorIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *sensorPPSLabel;
@property (weak, nonatomic) IBOutlet UILabel *sensorRSSILabel;

@end

@implementation SensorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [_sensor removeSensorDataObserver:self];
    
}

- (void)setSensor:(NPESensorConnection *)sensor {
    if (sensor != nil) {
        _sensor = sensor;

    }
    
    self.sensorIDLabel.text = _sensor.deviceIDString;
    self.sensorPPSLabel.text = @"0.0 pps";
    self.sensorRSSILabel.text = @"0 dB";

    
    __weak typeof(self) weakSelf = self;
    [_sensor addSensorDataObserver:self block:^(NPESensorData *data) {
        
        NSInteger rssi = [[data getParameterValueForKey:NPESensorDataParameterKeyRSSI  onlyIfValid:NO] integerValue];
        double pps = [[data getParameterValueForKey:NPESensorDataParameterKeyPPS onlyIfValid:NO] doubleValue];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.sensorPPSLabel.text = [NSString stringWithFormat:@"%2.2f pps", pps];
            weakSelf.sensorRSSILabel.text = [NSString stringWithFormat:@"%d dB", (int)rssi];
        });
        
    }];

}
@end
