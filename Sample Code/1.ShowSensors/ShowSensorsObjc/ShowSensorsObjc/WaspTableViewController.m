//
//  WaspTableViewController.m
//  ShowSensorsObjc
//
//  NPEGATE SAMPLE Copyright © 2012-2016 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  Joe Tretter
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
//

#import "WaspTableViewController.h"
#import "GatewayManager.h"

NSString * const WaspReuse = @"waspReuseIdentifier";

typedef NS_ENUM(NSInteger, TableSection) {
    TableSectionWaspB       = 0,
    TableSectionWaspN       = 1,
    TableSectionWaspPoe     = 2,
    TableSectionWaspSim     = 3,
    TableSectionWaspGem3Poe = 4,
    TableSectionNumItems
};

@interface WaspTableViewController ()


@end

@implementation WaspTableViewController
{
    UIAlertController *checkNetworkAlert;
    BOOL allowNetworkCheck;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    self.title = @"WASP";

    [NSTimer scheduledTimerWithTimeInterval:5.5 target:self selector:@selector(updateWaspDisplay:) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateNetworkDiagnostics) userInfo:nil repeats:YES];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([CLLocationManager locationServicesEnabled]){

        NSLog(@"Location Services Enabled");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Permission Denied" message:@"To re-enable, please go to Settings and turn on Location Service for this app." preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                    if (success) {
                        NSLog(@"Opened url:%@",UIApplicationOpenSettingsURLString);
                    }
                }];
            } else {
                [application openURL:URL
                             options: @{}
                   completionHandler:^(BOOL success) {
                    NSLog(@"Open %@: %d",UIApplicationOpenSettingsURLString,success);
                }];
            }
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];

        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            
            [self presentViewController:alertController animated:YES completion:^{
                            
            }];
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateWaspDisplay:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark - WASP Arrays for Tableview

- (NSArray<Wasp *> *)waspOnNetwork {

    NSArray<Wasp *> *known = [GatewayManager sharedManager].allWasps;
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    return [known sortedArrayUsingDescriptors:descriptors];
}

- (NSArray<Wasp *> *)waspBArray {

    NSArray *waspB = [self.waspOnNetwork filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"hardwareType = %d", WaspProductTypeWaspB]];

    return waspB;
}

- (NSArray<Wasp *> *)waspNArray {

    NSArray *pWasp = [[[NPEHardwareConnector sharedConnector] knownWasps] allObjects];

    NSPredicate *waspNPredicate = [NSPredicate predicateWithBlock:^BOOL(Wasp *evaluatedObject, NSDictionary *bindings) {

        return (evaluatedObject.hardwareType == WaspProductTypeWaspN ||
                evaluatedObject.hardwareType == WaspProductTypeWaspN_N550 ||
                evaluatedObject.hardwareType == WaspProductTypeWaspN_N550_N1);

    }];


    NSArray *wasps = [pWasp filteredArrayUsingPredicate:waspNPredicate];

    return wasps;
}

- (NSArray<Wasp *> *)waspPoEArray {

    NSPredicate *waspPoePredicate = [NSPredicate predicateWithBlock:^BOOL(Wasp *evaluatedObject, NSDictionary *bindings) {

        return (evaluatedObject.hardwareType == WaspProductTypeWaspPoE_STM32F107_C7 ||
                evaluatedObject.hardwareType == WaspProductTypeWaspPoE_STM32F107_N548 ||
                evaluatedObject.hardwareType == WaspProductTypeWaspPoE_STM32F107_N550);

    }];


    NSArray *wasps = [self.waspOnNetwork filteredArrayUsingPredicate:waspPoePredicate];

    return wasps;
}

- (NSArray<Wasp *> *)waspGem3PoEArray {

    NSPredicate *waspPoePredicate = [NSPredicate predicateWithBlock:^BOOL(Wasp *evaluatedObject, NSDictionary *bindings) {

        return (evaluatedObject.hardwareType == WaspProductTypeGEM3POE ||
                evaluatedObject.hardwareType == WaspProductTypeWasp3_STM32F107_GEM3 ||
                evaluatedObject.hardwareType == WaspProductTypeWasp3_STM32H753_GEM3);

    }];


    NSArray *wasps = [self.waspOnNetwork filteredArrayUsingPredicate:waspPoePredicate];

    return wasps;
}


- (NSArray<Wasp *> *)waspSimArray {

    NSArray *waspSim = [self.waspOnNetwork filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"hardwareType = %d", WaspProductTypeWASPSimulator]];
    
    return waspSim;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return TableSectionNumItems;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case TableSectionWaspB:
            return [self waspBArray].count;
            break;
        case TableSectionWaspN:
            return [self waspNArray].count;
            break;
        case TableSectionWaspPoe:
            return [self waspPoEArray].count;
            break;
        case TableSectionWaspGem3Poe:
            return [self waspGem3PoEArray].count;
            break;
        case TableSectionWaspSim:
            return [self waspSimArray].count;
            break;

        default:
            break;
    }

    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    switch (section) {
        case TableSectionWaspB:
            return [self waspBArray].count > 0 ? @"WASP-B" : nil;
            break;
        case TableSectionWaspN:
            return [self waspNArray].count > 0 ? @"WASP-N" : nil;
            break;
        case TableSectionWaspPoe:
            return [self waspPoEArray].count > 0 ? @"WASP-PoE" : nil;
            break;
        case TableSectionWaspGem3Poe:
            return [self waspGem3PoEArray].count > 0 ? @"WASP GEM3-PoE" : nil;
            break;
        case TableSectionWaspSim:
            return [self waspSimArray].count > 0 ? @"WASP Simulator" : nil;
            break;

        default:
            break;
    }

    return nil;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:WaspReuse forIndexPath:indexPath];
    
    if ([self.waspOnNetwork count] > indexPath.row) {

        Wasp *wasp;

        switch (indexPath.section) {
            case TableSectionWaspB:
                wasp = [self waspBArray][indexPath.row];
                break;
            case TableSectionWaspN:
                wasp = [self waspNArray][indexPath.row];
                break;
            case TableSectionWaspPoe:
                wasp = [self waspPoEArray][indexPath.row];
                break;
            case TableSectionWaspGem3Poe:
                wasp = [self waspGem3PoEArray][indexPath.row];
                break;
            case TableSectionWaspSim:
                wasp = [self waspSimArray][indexPath.row];
                break;
        }

        cell.textLabel.text = wasp.name;
        cell.detailTextLabel.text = wasp.ipAddress;

    }

    return cell;
}

-(void)updateNetworkDiagnostics
{
    
    if ([[UIApplication sharedApplication] applicationState]==UIApplicationStateActive &&
        allowNetworkCheck==YES &&
        checkNetworkAlert.isBeingPresented == NO &&
        [NPEHardwareConnector sharedConnector].networkLatency.doubleValue>5000.0) // Looking for no trafffic for 5 seconds
    {
        checkNetworkAlert = [UIAlertController alertControllerWithTitle:@"LOCAL NETWORK NOT ACCESSIBLE" message:@"Check the Local Network Settings to ensure it is enabled.  The Local Network must be enabled for WASP traffic to be accessible to the App." preferredStyle:UIAlertControllerStyleAlert];
        [checkNetworkAlert addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [checkNetworkAlert addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self appSettings];
            [self  performSelector:@selector(delayedClear) withObject:nil afterDelay:2.0];
        }]];
        allowNetworkCheck = NO;
        [self presentViewController:checkNetworkAlert animated:YES completion:^{
            
        }];
    }
}

-(void)delayedClear
{
    allowNetworkCheck = YES;
}


- (void)appSettings
{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"Opened url:%@",UIApplicationOpenSettingsURLString);
            }
        }];
    } else {
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            NSLog(@"Open %@: %d",UIApplicationOpenSettingsURLString,success);
        }];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
