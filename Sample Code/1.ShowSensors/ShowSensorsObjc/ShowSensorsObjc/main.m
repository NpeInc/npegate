//
//  main.m
//  ShowSensorsObjc
//
//  Created by Kevin Hoogheem on 4/2/15.
//  Copyright (c) 2015 North Pole Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
